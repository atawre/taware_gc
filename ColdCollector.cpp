/*
 * Allocator.cpp
 *
 *  Created on: 28-Jan-2018
 *      Author: ataware
 */
#include<string>
#include <cstdio>
#include <cstdlib>
#include "Heap.h"
#include "Space.h"

int heapSize	= 640;
int liveFactor	= 70;
int heapGrowth	= 15;
int revitalized = 0;
unsigned edengcNum = 0;
unsigned maturegcNum = 0;
unsigned coldgcNum = 0;
unsigned curTime =	0;
unsigned coldnessThreshold = 10000;
std::set<int> rootSet;

void usage(char* argv[]){
	std::cout << argv[0] << " -s <heap-size> -t <live object %> -p <heap growth %> -c <coldness threshold(#lines)> <trace-file>\n";
}

int main(int argc, char* argv[]){
	if(argc!=10){
		usage(argv);
		return 1;
	}

	heapSize = atoi(argv[2]);
	liveFactor=atoi(argv[4]);
	heapGrowth=atoi(argv[6]);
	coldnessThreshold=atoi(argv[8]);

	Space *space = new Space(heapSize, COLDCOPY);

	std::ifstream infile(argv[9]);

	std::ofstream oss(space->getOutputFile());

	std::string line, field;
	char cmd;
	int threadId, id, childId, slot, size, nrefs, cid; //a Ti Oj Sk Nl Cj
	int addr=-1;

	unsigned gcNum=0, lineNum=0, allocations=0;

    oss << "\n\nParameters";
	oss << "\nheapSize : " << heapSize;
	oss << "\nliveFactor : " << liveFactor;
	oss << "\nheapGrowth : " << heapGrowth;
	oss << "\ncoldnessThreshold : " << coldnessThreshold;
    oss << std::endl;

    oss << "gcCount, edenBeforeGC, edenAfterGC, edenLive, maturegcNum, matureBeforeGC, matureAfterGC, matureLive, coldgcNum, coldBeforeGC, coldAfterGC, coldLive, revitalized, lineNum, allocations\n";
	std::cout << "gcNum fromBeforeGC fromAfterGC increment maturegcNum coldgcNum revitalized \n";
	while (std::getline(infile, line)){
		curTime = lineNum++;
		std::istringstream iss(line);
		iss >> cmd;
		try{
			switch(cmd){
			case '+':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				rootSet.insert(id);
				space->updateTime(id);
				//space->setColor(id, GREY);
				break;
			case '-':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				rootSet.erase(id);
				//std::cout << "Failed to remove " << id <<" from the root set.\n";
				break;
				//w T0 P2 #0 O1 F24 S8 V0
			case 'w':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> slot; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> childId; //parent object ID.

				space->addChild(id, childId, slot);

				break;
			case 'a':
				allocations++;
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> size;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> nrefs;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> cid;

				addr = space->allocateEden(size);
				if(addr==-1){
					gcNum++;
					throw(addr);
				}

				space->getEdenSpace()->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				break;
			default:
				break;
			}
		}
		catch(int err){
			unsigned edenBeforeGC = space->getEdenSpace()->getUsedMem();
			unsigned matureBeforeGC = space->getMatureSpace()->getUsedMem();
			unsigned coldBeforeGC = space->getColdSpace()->getUsedMem();

			space->gcEdenSpace();
			unsigned edenAfterGC = space->getEdenSpace()->getUsedMem();
			unsigned matureAfterGC = space->getMatureSpace()->getUsedMem();
			unsigned coldAfterGC = space->getColdSpace()->getUsedMem();

			int increment = 0;
			int heapSizebeforeGrow = space->getEdenSpace()->getSize();
			double usedPerc = edenAfterGC*100.0/heapSizebeforeGrow;
			if(usedPerc >= liveFactor){
				increment = heapGrowth*heapSizebeforeGrow/100;
				//std::cout << gcNum << " ";
				space->grow();
			}

			addr = space->allocateEden(size);
			unsigned liveEden   = space->getEdenSpace()->getliveObjects();
			unsigned liveMature = space->getMatureSpace()->getliveObjects();
			unsigned liveCold   = space->getColdSpace()->getliveObjects();

			if(addr!=-1){
				space->getEdenSpace()->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				oss << gcNum <<"," << edenBeforeGC <<"," << edenAfterGC << "," << liveEden <<"," << maturegcNum <<"," << matureBeforeGC <<"," << matureAfterGC << "," << liveMature <<","  << coldgcNum <<","  << coldBeforeGC <<"," << coldAfterGC << "," << liveCold <<"," << revitalized <<"," << lineNum <<"," <<allocations <<"\n";
				std::cout << gcNum << " " << edenBeforeGC << " " << edenAfterGC << " " << increment << " " << maturegcNum << " " << coldgcNum << " " << revitalized << "\n";
			}else {
				std::cout << lineNum << ":Allocation(2) failed second time. Exiting..\n";
				oss << gcNum <<"," << edenBeforeGC <<"," << edenAfterGC << "," << liveEden <<"," << maturegcNum <<"," << matureBeforeGC <<"," << matureAfterGC << "," << liveMature <<","  << coldgcNum <<","  << coldBeforeGC <<"," << coldAfterGC << "," << liveCold <<"," << revitalized <<"," << lineNum <<"," <<allocations <<"\n";
				std::cout << gcNum << " " << edenBeforeGC << " " << edenAfterGC << " " << increment << " " << maturegcNum << coldgcNum << " " << revitalized << "\n";
				break;
			}
		}
	}

	oss.close();
	infile.close();

    std::cout << "\n------------------------------------------";
    std::cout << "\nEden object stats.";
    std::cout << "\n------------------------------------------";
	space->getEdenSpace()->stat();

    std::cout << "\n------------------------------------------";
    std::cout << "\nMature object stats.";
    std::cout << "\n------------------------------------------";
	space->getMatureSpace()->stat();

    std::cout << "\n------------------------------------------";
    std::cout << "\nCold object stats.";
    std::cout << "\n------------------------------------------";
    space->getColdSpace()->stat();
    std::cout << std::endl;
	return 0;
}
