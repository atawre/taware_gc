/*
 * Allocator.cpp
 *
 *  Created on: 28-Jan-2018
 *      Author: ataware
 */
#include<string>
#include <cstdio>
#include <cstdlib>
#include "Heap.h"
#include "Space.h"

int heapSize=640;
int liveFactor=70;
int heapGrowth=15;
int revitalized = 0;
unsigned edengcNum = 0;
unsigned maturegcNum = 0;
unsigned coldgcNum = 0;
unsigned curTime =	0;
unsigned coldnessThreshold = 10000;
std::set<int> rootSet;

void usage(char* argv[]){
	std::cout << argv[0] << " -s <heap-size> -t <live object %> -p <heap growth %> <trace-file>\n";
}

int main(int argc, char* argv[]){
	if(argc!=8){
		usage(argv);
		return 1;
	}

	heapSize = atoi(argv[2]);
	liveFactor=atoi(argv[4]);
	heapGrowth=atoi(argv[6]);

	Space *space = new Space(heapSize, COPY);

	std::ifstream infile(argv[7]);

	std::ofstream oss(space->getOutputFile());

	std::string line, field;
	char cmd;
	int threadId, id, childId, slot, size, nrefs, cid; //a Ti Oj Sk Nl Cj
	int addr=-1;

	unsigned gcNum=0, lineNum=0, allocations=0;

	oss << "gcCount, bytesUsedBeforeGC, bytesUsedAfterGC, liveObjects, lineNum, allocations\n";

	while (std::getline(infile, line)){
		curTime = lineNum++;
		std::istringstream iss(line);
		iss >> cmd;
		try{
			switch(cmd){
			case '+':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				rootSet.insert(id);
				break;
			case '-':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				rootSet.erase(id);
				//std::cout << "Failed to remove " << id <<" from the root set.\n";
				break;
				//w T0 P2 #0 O1 F24 S8 V0
			case 'w':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> slot; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> childId; //parent object ID.

				if(space->getFromSpace()->exists(id))
					space->getFromSpace()->addChild(id, childId, slot);
				//else
				//	std::cout << "Object "<< id <<" is not allocated. Skipping write command.\n";
				break;
			case 'a':
				allocations++;
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> size;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> nrefs;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> cid;

				//addr = heap->getFromSpace()->allocate(size);
				addr = space->allocateFrom(size);
				if(addr==-1){
					gcNum++;
					throw(addr);
				}

				space->getFromSpace()->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				break;
			default:
				break;
			}
		}
		catch(int err){
			int bytesUsedBeforeGC = space->getFromSpace()->getUsedMem();
			space->gcCopyCollector(rootSet);
			int bytesUsedAfterGC = space->getFromSpace()->getUsedMem();

			int increment = 0;
			int heapSizebeforeGrow = space->getFromSpace()->getSize();
			double usedPerc = bytesUsedAfterGC*100.0/heapSizebeforeGrow;
			if(usedPerc >= liveFactor){
				increment = heapGrowth*heapSizebeforeGrow/100;
				space->grow();
			}

			addr = space->allocateFrom(size);
			if(addr!=-1){
				space->getFromSpace()->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				std::cout << gcNum << " " << bytesUsedBeforeGC << " " << bytesUsedAfterGC << " " << increment << "\n";
				oss << gcNum <<"," <<bytesUsedBeforeGC <<"," << bytesUsedAfterGC << "," << space->getFromSpace()->getliveObjects() <<"," << lineNum <<"," <<allocations <<"\n";
			}else {
				std::cout << lineNum << ":Allocation(2) failed second time. Exiting..\n";
				std::cout << gcNum << " " << bytesUsedBeforeGC << " " << bytesUsedAfterGC << " " << increment << "\n";
				oss << gcNum <<"," <<bytesUsedBeforeGC <<"," << bytesUsedAfterGC << "," << space->getFromSpace()->getliveObjects() <<"," << lineNum <<"," <<allocations <<"\n";
                break;
			}
		}
	}
	oss.close();
	infile.close();
	space->getFromSpace()->stat();
	return 0;
}
