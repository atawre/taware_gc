/*
 * Heap.cpp
 *
 *  Created on: 16-Jan-2018
 *      Author: ataware
 */

#include <cstdlib>
#include "Heap.h"

extern int curTime;

Heap::Heap(int start, int sz, collector type){
	freeMem = size = sz;
	startAddr = start;
	collectorType = type;
	//liveObjects = 0;
	if(sz>0)
		freeList.push_back(std::make_pair(startAddr, size));
}

Heap::~Heap() {
	freeList.clear();
}

const char* Heap::getOutputFile(collector c){
    switch (c)
    {
        case MARKSWEEP:return "mark_sweep.csv";
        case COPY:   	return "copy.csv";
        case REFCOUNT:	return "refcount.csv";
        case RECYCLER:	return "recycler.csv";
        case COLDCOPY:	return "coldcopy.csv";
    }
}

bool Heap::exists(int id){
	if(objMap.count(id)>0)
		return true;
	else
		return false;
}

void Heap::setTime(int id){
	if(exists(id))
		objMap[id]->timeStamp  = curTime;
}

unsigned Heap::getTime(int id){
	return objMap[id]->timeStamp;
}

int Heap::getUsedMem(){
	return(size-freeMem);
}

int Heap::getFreeMem(){
	return freeMem;
}

int Heap::getSize(){
	return size;
}

int Heap::getStartAddr(){
	return startAddr;
}

objectAllocationMap& Heap::getObjMap(){
	return objMap;
}

void Heap::clear(){
	for(objectAllocationMap::iterator it=objMap.begin(); it!=objMap.end(); ++it)
		delete(it->second);
	objMap.clear();
	freeList.clear();
	freeMem = size;
	freeList.push_back(std::make_pair(startAddr, size));
}

int Heap::getliveObjects(){
	return objMap.size();
}

void Heap::showFreeList() {
	for(std::list < std::pair<int, int> >::iterator it=freeList.begin(); it!=freeList.end();++it)
		std::cout << "("<< it->first << ", " << it->second << ")=>";
	std::cout << std::endl;
}

int Heap::getUsagePercentage(){
	int percHeapUse = ((size - freeMem)*100.0)/size;
	return percHeapUse;
}

void Heap::stat() {
	//a. Percentage of the heap used
	//b. Number in the free list
	//c. Average size in the free list
	double percHeapUse, avgFreeSize;
	std::list < std::pair<int, int> >::iterator it,prev;
	int freeNodes = freeList.size();
	percHeapUse = avgFreeSize = 0.0;

	percHeapUse = ((size - freeMem)*100.0)/size;
	avgFreeSize = (float)freeMem/freeNodes;

	std::cout << "\nHeap Size                 : " << size;
	std::cout << "\nPercentage heap usage     : " << std::setprecision(3) << percHeapUse;
	std::cout << "\nTotal Free Nodes          : " << freeNodes;
	std::cout << "\nAverage size in free list : " << std::setprecision(3) << avgFreeSize;
}

bool Heap::free(int addr, int size){
	std::list < std::pair<int, int> >::iterator it,prev;

	if(freeList.empty()){
		freeList.push_back(std::make_pair(addr, size));
		freeMem += size;
		return true;
	}

	prev=freeList.begin();
	for(it=freeList.begin(); it!=freeList.end();++it){
		if(addr > it->first){
			//std::cout << "prev : " << prev->first << ", " << prev->second << "\n";
			prev = it;
		}else if(addr < it->first){
			//std::cout << "curr : " << it->first << ", " << it->second << "\n";
			break;
		}else{ //Double free?
			return false;
		}
	}

	int prevFreeBlockEndAddr = prev->first + prev->second;
	int nextFreeBlockStartAddr = -1;
	if(it!=freeList.end())
		nextFreeBlockStartAddr = it->first;

	if(addr==prevFreeBlockEndAddr){
		if(it==freeList.end()){		//this is suffix to last free Node.
			prev->second += size;
		}else if(addr+size==nextFreeBlockStartAddr){ //it!=prev
			//merge consecutive free blocks
			prev->second = prev->second + size + it->second;
			freeList.erase(it);
		}else if(addr+size < nextFreeBlockStartAddr){ //it!=prev
			//increase size of prev free block
			prev->second += size;
		}else{
			std::cout << "free: invalid request.\n";
			return false;
		}
	}else{
		if(addr+size==nextFreeBlockStartAddr){ //it!=prev
			//merge freed block with next available free block.
			it->first = addr;
			it->second += size;
		}else{
			//create new free block
			if(it==freeList.end())  //append
				freeList.push_back(std::make_pair(addr, size));
			else					//insert
				freeList.insert(it, std::make_pair(addr, size));
		}
	}
	freeMem += size;
	return true;
}

int Heap::allocate(int numBytes) {
	std::list < std::pair<int, int> >::iterator it, prev;
	int addr=-1;
	for(it=freeList.begin(); it!=freeList.end();++it){
		if(numBytes == it->second){
			addr = it->first;
			prev = it++;
			freeList.erase(prev); 	//use all the memory in this block.
			freeMem -= numBytes;
			return addr;
		}else if(numBytes < it->second) {
			addr = it->first;
			it->first += numBytes;	//increment address.
			it->second -= numBytes;	//reduce available size in free block.
			freeMem -= numBytes;
			return addr;
		}
	}
	return addr;
}

int Heap::firstFreeAddress(){
	return freeList.begin()->first;
}

int Heap::shrink(int increment){
	//dummy function.
	freeMem -= increment;
	size -= increment;
	return increment;
}

int Heap::grow(int increment){
	std::list < std::pair<int, int> >::iterator last;

	if(increment<=0) //invalid request.
		return 0;

	if(freeList.empty()){	//add new block
		freeList.push_back(std::make_pair(size, increment));
		size += increment;
		freeMem += increment;
		return increment;
	}

	last = freeList.end();
	last--;

	if(last->first+last->second==size){  //merge with last block on the boundary.
		last->second += increment;
		size += increment;
		freeMem += increment;
		return increment;
	}

	freeList.push_back(std::make_pair(size, increment)); //append new block
	size += increment;
	freeMem += increment;
	return increment;
}

int Heap::gcRefCount(std::set<int> rootSet){
	int freedMem = 0;
	return gcMarkSweep(rootSet);
}

std::list<Object*> workList;

int Heap::gcMarkSweep(std::set<int> rootSet){
	//liveObjects=0; //reset live object count.
	workList.clear();
	for(std::set<int>::iterator it=rootSet.begin(); it!=rootSet.end(); it++){
		if(objMap.count(*it)>0){
			Object *root = objMap[*it];
			root->setMark();
			workList.push_back(root);
			mark();
		}
	}
	return sweep();
}

void Heap::mark(){
	while(!workList.empty()){
		Object *parent = workList.front();
		workList.pop_front();
		int *children = parent->getChildren();
		for(int i=0; i<parent->getNRefs(); i++){
			if(children[i]!=-1 && objMap.count(children[i])>0){
				Object *child = objMap[children[i]];
				if(!child->marked()){
					child->setMark();
					workList.push_back(child);
				}
			}
		}
	}
}

int Heap::sweep(){
	int freedMem=0;
	for(objectAllocationMap::iterator it=objMap.begin(); it!=objMap.end(); it++) {
		Object* obj = it->second;
		if(obj->marked()){
			obj->unsetMark();
			//liveObjects++;
		}else{
			freedMem += obj->getSize();
			free(obj->getAddr(), obj->getSize());
			delete(obj);
			objMap.erase(it--);
		}
	}
	return freedMem;
}

int Heap::addChild(int parentId, int childId, int slot){
	if(!objMap.count(parentId))
		return -1;

	int *children = objMap[parentId]->getChildren();

	int prevChildId = children[slot];	//save old child ID.

	//update time stamps.
//	setTime(prevChildId);
	setTime(parentId);
//	setTime(childId);

	objMap[parentId]->addChild(slot, childId);
	if(collectorType==RECYCLER)
		addReference(childId);
	else if(collectorType==REFCOUNT)
		incRefCount(childId);

	if(prevChildId!=-1){
		if(collectorType==RECYCLER)
			deleteReference(prevChildId);
		else if(collectorType==REFCOUNT)
			decRefCount(prevChildId);
	}
	return prevChildId;
}

void Heap::printCandidates(){
	std::cout << "\nCandidates : ";
	for(int i : candidates){
		objMap[i]->displayColor();
	}
	std::cout << "\n";
}

void Heap::addObject(int id, Object *obj){
	if(0==objMap.count(id)){
		objMap[id] = obj;
	}else
		std::cout << "Object with id " << id << " is already allocated.\n";
}

void Heap::removeObject(int id, bool deep=true){
	if(!objMap.count(id)){
		std::cout << "Object with id " << id << " does not exist.\n";
		return;
	}

    Object *freeMe = objMap[id];
    //if(collectorType!=COPY)
    //don't need this for copy collector.
    free(freeMe->getAddr(), freeMe->getSize());

    if(collectorType==REFCOUNT){
		int *children = freeMe->getChildren();
		for(int i=0; i<freeMe->getNRefs() && children[i]!=-1; i++)
			decRefCount(children[i]);
    }

	if(deep)
		delete(freeMe);
	objMap.erase(id);           //remove ID => Object binding too.
}

Object* Heap::getObject(int id){
	if(exists(id))
		return objMap[id];
	else
		return nullptr;
}

void Heap::showAllocationMap(){
	std::cout << "----------------------------------------" << std::endl;
	std::cout << "Map (objectId => Object)" << std::endl;
	std::cout << "----------------------------------------" << std::endl;

	for(objectAllocationMap::iterator i=objMap.begin(); i!=objMap.end(); ++i){
		std::cout << "m:" << i->second->marked() << " id:" << i->first << "=> ";
		int *children = i->second->getChildren();
		for(int j=0; j<i->second->getNRefs(); j++)
			if(children[j]!=-1)
				std::cout << children[j] <<"(" << objMap[children[j]]->marked()<<") ";
		std::cout << "\n";
	}
	std::cout << std::endl;
}

void Heap::incRefCount(int id){
	if(!objMap.count(id))
		return;
	objMap[id]->incRefCount();
}

void Heap::decRefCount(int id){
	if(!objMap.count(id))
		return;
	if(!objMap[id]->decRefCount())
		removeObject(id);			//if refcount drops to zero, remove it.
}

void Heap::addReference(int id){
	if(objMap.count(id)==0)
		return;
	objMap[id]->incRefCount();
	objMap[id]->setColor(BLACK);
}

void Heap::deleteReference(int id){
	if(objMap.count(id)==0)
		return;
	if(!objMap[id]->decRefCount())
		release(id);
	else
		candidate(id);		/* might isolate a garbage cycle */
}

void Heap::release(int id){
	if(!objMap.count(id)){
		std::cout << "Object with id " << id << " does not exist.\n";
		return;
	}

	Object *freeMe = objMap[id];
	int *children  = freeMe->getChildren();
	for(int i=0; i<freeMe->getNRefs() && children[i]!=-1; i++)
		deleteReference(children[i]);

	objMap[id]->setColor(BLACK);			/* objects on the free—list are black */
	if(!candidates.count(id)){
		free(freeMe->getAddr(), freeMe->getSize());
		delete(freeMe);
		objMap.erase(id);			//remove ID => Object binding too.
	}
}

void Heap::candidate(int id){
	if(objMap[id]->getColor() != PURPLE){
		objMap[id]->setColor(PURPLE);
		candidates.insert(id);
	}
}

void Heap::recyclerCollect(){
	markCandidates();
	for(int id: candidates){
		scan(id);
	}
	collectCandidates();
}

void Heap::markCandidates(){
	for(int id: candidates){
		if(0==objMap.count(id))
			continue;

		if(objMap[id]->getColor()==PURPLE){
			markGrey(id);
		}else{
			candidates.erase(id);
			if(objMap[id]->getColor()==BLACK && objMap[id]->getRefCount()==0){
				Object *freeMe = objMap[id];
				free(freeMe->getAddr(), freeMe->getSize());
				delete(freeMe);
				objMap.erase(id);
			}
		}
	}
}


void Heap::markGrey(int id){
	if(objMap.count(id) == 0 || objMap[id]->getColor()==GREY)
		return;

	objMap[id]->setColor(GREY);

	Object *o = objMap[id];
	int *children = o->getChildren();
	for(int i=0; i < o->getNRefs(); i++){
		int childId = children[i];
		if(childId!=-1 && objMap.count(childId)>0){
			objMap[childId]->decRefCount();
			markGrey(childId);
		}
	}
}

void Heap::scan(int id){
	if(objMap.count(id) == 0 || objMap[id]->getColor()!=GREY)
		return;
	Object *o = objMap[id];
	if(o->getRefCount() > 0){
		scanBlack(id);				/* there must be an external reference */
	}else{
		o->setColor(WHITE);						/* looks like garbage... */
		int *children = o->getChildren();		/* .. .so continue */
		for(int i=0; i < o->getNRefs(); i++)
			if(children[i]!=-1 && objMap.count(children[i]) > 0)
				scan(children[i]);
	}
}

void Heap::scanBlack(int id){
	Object *o = objMap[id];
	o->setColor(BLACK);
	int *children = o->getChildren();
	for(int i=0; i < o->getNRefs(); i++)
		if(children[i]!=-1 && objMap.count(children[i]) > 0){
			Object *child = objMap[children[i]];
			child->incRefCount();			/* undo the trial deletion */
			if(child->getColor()!=BLACK)
				scanBlack(children[i]);
		}
}

void Heap::collectCandidates(){
	std::set<int>::iterator it = candidates.begin();
    while(it != candidates.end()){
    	int id = *it;
    	candidates.erase(it++);
    	collectWhite(id);
    }
}

void Heap::collectWhite(int id){
	if(!objMap.count(id))
		return;
	Object *o = objMap[id];
	if(o->getColor()!=WHITE || candidates.count(id))
		return;

	o->setColor(BLACK);					/* free—list objects are black */
	int *children = o->getChildren();
	for(int i=0; i < o->getNRefs(); i++)
		if(children[i]!=-1 && objMap.count(children[i]) > 0)
			collectWhite(children[i]);

	free(o->getAddr(), o->getSize());
	delete(o);
	objMap.erase(id);
}

void Heap::setColor(int id, Color c){
	objMap[id]->setColor(c);
}

Color Heap::getColor(int id){
	return objMap[id]->getColor();
}

