/*
 * Heap.h
 *
 *  Created on: 16-Jan-2018
 *      Author: ataware
 */

#ifndef HEAP_H_
#define HEAP_H_
#include <list>
#include <utility>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <iomanip>
#include <set>
#include "Object.h"

#define ALLOCATE 1
#define FREE 0
#define MARK_SWEEP_COLLECTOR 1
#define COPY_COLLECTOR 2

typedef std::map<int, Object*> objectAllocationMap; //objectID, Object

/*
Color Meaning
Black 	In use or free
Gray 	Possible member of cycle
White 	Member of garbage cycle
Purple 	Possible root of cycle
Green 	Acyclic
 */

enum collector{
	MARKSWEEP,
	COPY,
	REFCOUNT,
	RECYCLER,
	COLDCOPY
};

class Heap {
private:
	int freeMem;
	int size;
	int startAddr;		//pointers for copy collector.
	collector collectorType;
	//int liveObjects;
	std::list < std::pair<int, int> > freeList;
	objectAllocationMap objMap;
	std::set<int> candidates;
	std::set<int> remset;

	//std::map<int, int> addrMap;	//map of address => objectId (this is sorted by address.)
public:
	Heap(int start, int sz, collector type);
	virtual ~Heap();

	const char *getOutputFile(collector);
	int getUsedMem();
	int getFreeMem();
	int getSize();
	int getUsagePercentage();
	int getStartAddr();
	int getliveObjects();

	void clear();

	int firstFreeAddress();
	int allocate(int numBytes);
	bool free(int addr, int size);

	int grow(int increment);
	int shrink(int increment);

	void addObject(int id, Object *o);
	void removeObject(int id, bool deep);
	Object* getObject(int id);

	bool isPreset(int id);
	int addChild(int parentId, int childId, int slot);

	void stat();
	void showFreeList();
	void showAllocationMap();

	int gcMarkSweep(std::set<int> rootSet);
	void mark();
	int sweep(); //return total memory garbage collected.

	int gcRefCount(std::set<int> rootSet);

	void incRefCount(int id);
	void decRefCount(int id);

	void addReference(int id);
	void deleteReference(int id);

	void release(int id);
	void candidate(int id);
	void recyclerCollect();
	void markCandidates();
	void markGrey(int id);

	void scan(int id);
	void scanBlack(int id);
	void collectCandidates();
	void collectWhite(int id);

	objectAllocationMap& getObjMap();
	std::map<int, int>& getAddrMap();

	bool exists(int id);

	void setColor(int , Color);
	Color getColor(int);

	void setTime(int);
	unsigned getTime(int);

	void printCandidates();

};

#endif /* HEAP_H_ */
