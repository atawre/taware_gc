/*
 * Allocator.cpp
 *
 *  Created on: 28-Jan-2018
 *      Author: ataware
 */
#include<string>
#include "Heap.h"
#include <cstdio>
#include <cstdlib>

int heapSize=500;
int liveFactor=50;
int heapGrowth=10;
int curTime=0;

void usage(char* argv[]){
	std::cout << argv[0] << " -s <heap-size> -t <live object %> -p <heap growth %> <trace-file>\n";
}

int main(int argc, char* argv[]){
	if(argc!=8){
		usage(argv);
		return 1;
	}

	heapSize = atoi(argv[2]);
	liveFactor=atoi(argv[4]);
	heapGrowth=atoi(argv[6]);

	Heap *heap = new Heap(0, heapSize, MARKSWEEP);
	std::ifstream infile(argv[7]);
	std::ofstream oss(heap->getOutputFile(MARKSWEEP));

	std::set<int> rootSet;

	std::string line, field;
	char cmd;
	int threadId, id, childId, slot, size, nrefs, cid; //a Ti Oj Sk Nl Cj
	int addr=-1;
	std::pair<std::set<int>::iterator,bool> ret;

	int gcNum=0, lineNum=0, allocations=0;

	oss << "gcCount, percentageUseAfterGC, bytesUsedBeforeGC, bytesUsedAfterGC, liveObjects, freedBytes, lineNum, allocations, heapGrowth\n";

	while (std::getline(infile, line)){
		curTime = lineNum++;
		std::istringstream iss(line);
		iss >> cmd;
		try{
			switch(cmd){
			case '+':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				ret = rootSet.insert(id);
				//if(!ret.second)
				//	std::cout << "Object " << id <<" already exists in root set.\n";
				break;
			case '-':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;
				rootSet.erase(id);
				//std::cout << "Failed to remove " << id <<" from the root set.\n";
				break;
				//w T0 P2 #0 O1 F24 S8 V0
			case 'w':
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> slot; //parent object ID.

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> childId; //parent object ID.

				if(heap->exists(id))
					heap->addChild(id, childId, slot);
				else
					std::cout << "Object "<< id <<" is not allocated. Skipping write command.\n";
				break;
			case 'a':
				allocations++;
				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> threadId;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> id;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> size;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> nrefs;

				iss >> field;
				field.erase(0, 1);
				std::istringstream(field) >> cid;

				addr = heap->allocate(size);
				if(addr==-1){
					gcNum++;
					throw(addr);
				}

				heap->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				break;
			default:
				break;
			}
		}
		catch(int err){
			//std::cout << "Thread T"<< threadId <<" failed to allocate " << size <<" bytes for object "<< id << ".\n";
			int bytesUsedBeforeGC = heap->getUsedMem();
			int freedBytes = heap->gcMarkSweep(rootSet);
			int bytesUsedAfterGC = heap->getUsedMem();
			int increment = 0;

			int heapSizebeforeGrow = heap->getSize();
			double usedPerc = bytesUsedAfterGC*100.0/heapSizebeforeGrow;
			if(usedPerc >= liveFactor){
				increment = heapGrowth*heapSizebeforeGrow/100;
				heap->grow(increment);
			}

			addr = heap->allocate(size);
			if(addr!=-1){
				heap->addObject(id, new Object(threadId, id, cid, size, nrefs, addr, curTime));
				std::cout << gcNum << " " << bytesUsedBeforeGC << " " << bytesUsedAfterGC << " " << increment << "\n";
				oss << gcNum <<"," <<usedPerc <<"," <<bytesUsedBeforeGC <<"," << bytesUsedAfterGC << "," <<heap->getliveObjects() <<"," <<freedBytes <<","  <<lineNum <<"," <<allocations <<"," <<increment <<"\n";
			}else {
				std::cout << lineNum << ":Allocation failed second time. Ignoring..\n";
				std::cout << gcNum << " " << bytesUsedBeforeGC << " " << bytesUsedAfterGC << " " << increment << "\n";
				oss << gcNum <<"," <<usedPerc <<"," <<bytesUsedBeforeGC <<"," << bytesUsedAfterGC << "," <<heap->getliveObjects() <<"," <<freedBytes <<","  <<lineNum <<"," <<allocations <<"," <<increment <<"\n";
				break;
			}
		}
	}
	oss.close();
	infile.close();
	heap->stat();
	return 0;
}
