/*
 * Object.cpp
 *
 *  Created on: 27-Jan-2018
 *      Author: ataware
 */

#include "Object.h"
#include <iostream>

Object::Object(int nrefs) {
	this->children = new int[nrefs];
	for(int i=0; i<nrefs; i++){
		this->children[i] = -1;
	}
	this->refcount = 0;
	this->age = this->timeStamp = 0;
	this->color = BLACK;
}

Object::Object(Object* copy) {
	this->tid = copy->tid;
	this->id = copy->id;
	this->cid = copy->cid;
	this->size = copy->size;
	this->nrefs = copy->nrefs;
	this->addr = copy->addr;
	//this->moved = false;	//will be true when it is moved.
	this->newAddr = copy->newAddr;		//initially it is undefined, till we move it across the spaces.
	this->markBit = copy->markBit;	//default creation with false value.
	this->children = new int[nrefs];
	for(int i=0; i<nrefs; i++){
		this->children[i] = copy->children[i];
	}
	this->refcount = 0;
	this->age = copy->age;
	this->timeStamp = copy->timeStamp;
	this->color = copy->color;
}

Object::Object(int tid, int id, int cid, int size, int nrefs, int addr, unsigned timeStamp) {
	this->tid = tid;
	this->id = id;
	this->cid = cid;
	this->size = size;
	this->nrefs = nrefs;
	this->addr = addr;
	this->moved = false;	//will be true when it is moved.
	this->newAddr = -1;		//initially it is undefined, till we move it across the spaces.
	this->markBit = false;	//default creation with false value.
	this->children = new int[nrefs];
	for(int i=0; i<nrefs; i++){
		this->children[i] = -1;
	}
	this->refcount = 0;
	this->age = 0;
	this->timeStamp = timeStamp;
	this->color = BLACK;
}

Object::~Object() {
	if(this->children)
		delete this->children;
}

void Object::show() {
	std::cout << "Thread" << tid << " obj:" << id << " children=> ";
	for(int i=0; i<nrefs; i++){
		if(children[i]!=-1)
		std::cout << children[i] << "(" << markBit << ") ";
	}
	std::cout << std::endl;
}

int Object::getId() {
	return this->id;
}

int Object::getAddr() {
	return this->addr;
}

void Object::setAddr(int addr) {
	this->addr = addr;
}

int Object::getNewAddr() {
	return this->newAddr;
}

void Object::setNewAddr(int newAddr) {
	this->newAddr = newAddr;
}

int Object::getSize() {
	return this->size;
}

int Object::getNRefs() {
	return this->nrefs;
}

int Object::incAge() {
	this->age++;
	return this->age;
}

int Object::decAge() {
	this->age--;
	return this->age;
}

int Object::getAge() {
	return this->age;
}

int Object::incRefCount() {
	this->refcount++;
	return this->refcount;
}

int Object::decRefCount() {
	this->refcount--;
	return this->refcount;
}

int Object::getRefCount() {
	return this->refcount;
}

bool Object::isMoved() {
	return this->moved;
}

void Object::setMoved() {
	this->moved = true;
}

void Object::unsetMoved() {
	this->moved = false;
}

void Object::setMark() {
	this->markBit = true;
}

void Object::unsetMark() {
	this->markBit = false;
}

bool Object::marked() {
	return this->markBit;
}

Color Object::getColor() {
	return this->color;
}

void Object::setColor(Color c) {
	this->color = c;
}

void Object::displayColor() {
    switch (this->color)
    {
        case BLACK: std::cout << id << ":BLACK "; break;
        case WHITE: std::cout << id << ":WHITE "; break;
        case GREY: std::cout << id << ":GREY "; break;
        case PURPLE: std::cout << id << ":PURPLE "; break;
    }
}

void  Object::addChild(int slot, int childId){
	if(slot<this->nrefs && slot>-1){
		this->children[slot] = childId;
	}else{
		std::cout << "Object O"<< this->id << "has "<<this->nrefs<<" slots.\n";
	}
}

int *Object::getChildren(){
	return children;
}

void Object::printChildren(){
	std::cout << "\nID:" << id << "=>";
	for(int i=0; i<nrefs && children[i]!=-1; i++){
		std::cout << children[i] << ",";
	}
	std::cout << "\n";
}
