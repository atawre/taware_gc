/*
 * Object.h
 *
 *  Created on: 27-Jan-2018
 *      Author: ataware
 */

#ifndef OBJECT_H_
#define OBJECT_H_

enum Color {
	WHITE,
	GREY,
	BLACK,
	PURPLE
};

class Object {
	int tid;	//thread ID
	int id;		//object ID
	int cid;	//class ID
	int size;	//object size
	int nrefs;	//#of references
	int addr;
	bool moved;
	int newAddr;
	Color color;
	bool markBit;	// mark bit
	int *children;
	int refcount;
	int age;
	//std::list *c;
public:
	unsigned timeStamp;

	Object(int nrefs);
	Object(Object *);
	Object(int, int, int, int, int, int, unsigned);
	virtual ~Object();
	void show();

	void setMark();
	void unsetMark();
	bool marked();

	bool isMoved();
	void setMoved();
	void unsetMoved();

	int getId();
	int getAddr();
	void setAddr(int);

	int getNewAddr();
	void setNewAddr(int);

	int getSize();
	int getNRefs();

	int incRefCount();
	int decRefCount();
	int getRefCount();

	int incAge();
	int decAge();
	int getAge();

	Color getColor();
	void setColor(Color);
	void displayColor();

	void addChild(int slot, int childId);
	int *getChildren();
	void printChildren();
};

#endif /* OBJECT_H_ */
