# README #

This README documents assignments for CS6905.

### Project (cold object sequestrator/collector) ###
To compile run : 

  $cmake CMakeLists.txt
  
  $make datatemp
  
  Usage :
  $ ./datatemp 

./datatemp -s <heap-size> -t <live object %> -p <heap growth %> -c <coldness threshold(#lines)> <trace-file>


This will generate datatemp.csv as a result file. The result file contains most of the stats to analyze the results(headers provided).
The results for a file with 10 million+ lines are saved under project-results folder.
The same file took very long when ran with coldness threshold or 10k. The results saved are for threshold = 50k

### Reference Count ###

To compile run : 

  $cmake CMakeLists.txt
  
  $make;
  
  Usage :
  $ ./recycler

./recycler -s <heap-size> -t <live object %> -p <heap growth %> <trace-file>


### Output ###

$ ./refcount -s 10000 -t 50 -p 50 tc/trace.200k 

### gcNum  BytesBeforeGC BytesAfterGC Increment ###

1 9568 8168 5000

2 14928 12720 7500

3 22416 17464 11250
.

.

.



Above commands generates out.csv which has different stats at various stages of GC as follows -

gcCount, bytesUsedBeforeGC, bytesUsedAfterGC, lineNum, allocations, liveObjects, increment

