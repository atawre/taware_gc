/*
 * Space.cpp
 *
 *  Created on: 13-Feb-2018
 *      Author: ataware
 */

#include "Space.h"

/*
 * Checks if an object with objectId=id exists in any of the spaces.
 * Returns : True if exists, false otherwise.
 */
bool Space::exists(int id, spaceType t){
    switch (t)
    {
        case FROM:	if(fromSpace) return fromSpace->exists(id); break;
        case TO:  	if(toSpace) return toSpace->exists(id); break;
        case COLD:  return coldSpace->exists(id);
        case EDEN:  return edenSpace->exists(id);
        case MATURE:  return matureSpace->exists(id);
    }
    return false;
}

unsigned coldCount=0;
unsigned avgTime=0;

/*
 * Locates and retrieves an object from the heap objects.
 * Returns : pointer to the Object class.
 */
Object * Space::getObject(int id){
	objectAllocationMap map;
	if(exists(id, EDEN))
		map = edenSpace->getObjMap();
	else if(exists(id, MATURE))
		map = matureSpace->getObjMap();
	else if(exists(id, TO))
		map = toSpace->getObjMap();
	else if(exists(id, COLD))
		map = coldSpace->getObjMap();
	else if(exists(id, FROM))
		map = fromSpace->getObjMap();
	if(map.count(id)>0)
		return map[id];
	else
		return nullptr;
}

/*
 * Creates a link from parentId --> childId into location 'slot' of parent
 */
void Space::addChild(int parentId, int childId, int slot){
	int prevChild=-1;
	if(exists(parentId, EDEN))
		prevChild = edenSpace->addChild(parentId, childId, slot);
	else if(exists(parentId, MATURE))
		prevChild = matureSpace->addChild(parentId, childId, slot);
	else if(exists(parentId, COLD))
		prevChild = coldSpace->addChild(parentId, childId, slot);
	updateTime(prevChild);
	updateTime(childId);
}

/*
 * Garbage collects the cold space (uses copy collection technique.)
 */
int Space::gcColdSpace(){
	coldgcNum++;

	toSpace = new Heap(0, coldSpace->getSize(), COLDCOPY);
	//start with empty worklist.
	worklist.clear();

	for(int i:rootSet){
		if(exists(i, COLD))
			moveCold(i, -1);		//move will update worklist.
	}

	for(int i:remsetCold){
		if(!exists(i, EDEN) || !exists(i, MATURE))
			continue;
		Object *o = getObject(i);
		int *children = o->getChildren();
		int nrefs = o->getNRefs();

		for(int j=0; j<nrefs; j++)
			if(exists(children[j], COLD))
				moveCold(children[j], -1);		//move will update worklist.
	}

	while(!worklist.empty()){
		int id = *worklist.begin();
		worklist.pop_front();
		if(exists(id, TO))
			processMovedCold(id);
	}

	flipCold();
	return 0;
}

/*
 * Garbage collects the mature space (uses copy collection technique.)
 */
int Space::gcMatureSpace(){
	maturegcNum++;

	if(coldSpace->getUsagePercentage()>80)
		gcColdSpace();

	toSpace = new Heap(0, matureSpace->getSize(), COLDCOPY);
	//start with empty worklist.
	worklist.clear();

	for(int i:rootSet){
		if(exists(i, MATURE))
			moveMature(i, -1);		//move will update worklist.
	}

	for(int i:remsetCold){
		if(!exists(i, EDEN) || !exists(i, COLD))
			continue;
		Object *o = getObject(i);
		int *children = o->getChildren();
		int nrefs = o->getNRefs();

		for(int j=0; j<nrefs; j++)
			if(exists(children[j], MATURE))
				moveMature(children[j], -1);		//move will update worklist.
	}

	while(!worklist.empty()){
		int id = *worklist.begin();
		worklist.pop_front();
		if(exists(id, TO))
			processMovedMature(id);
	}

	flipMature();
	return 0;
}

/*
 * Garbage collects the eden space (uses copy collection technique.)
 */
int Space::gcEdenSpace(){
	edengcNum++;
	revitalized = 0;

	if(matureSpace->getUsagePercentage()>80)
		gcMatureSpace();

//	if(coldSpace->getUsagePercentage()>80)
//		gcColdSpace();

	toSpace = new Heap(0, edenSpace->getSize(), COLDCOPY);
	//start with empty worklist.
	worklist.clear();

	for(int i:rootSet){
		if(exists(i, EDEN))
			moveEden(i, -1);		//move will update worklist.
	}

	for(int i:remsetEden){
		if(!exists(i, MATURE) || !exists(i, COLD))
			continue;
		Object *o = getObject(i);
		int *children = o->getChildren();
		int nrefs = o->getNRefs();

		for(int j=0; j<nrefs; j++)
			if(exists(children[j], EDEN))
				moveEden(children[j], -1);		//move will update worklist.
	}

	while(!worklist.empty()){
		int id = *worklist.begin();
		worklist.pop_front();
		if(exists(id, TO))
			processMovedEden(id);
	}

	flipEden();
	return 0;
}

/*
 * Checks if the object has child in space 's'
 * Return : True/False
 */
bool Space::childLeftIn(Object *obj, spaceType s){
	int *children = obj->getChildren();
	int nrefs = obj->getNRefs();

	for(int i=0; i<nrefs; i++)
		if(exists(children[i], s))
			return true;
		else if(exists(children[i], TO))
			return true;
	return false;
}

/*
 *	Moves object from Eden space to either matureSpace, toSpace or coldSpace
 *	Returns : address of object in moved location.
 */
int Space::moveEden(int id, int parent=-1){
	Object *obj = edenSpace->getObject(id);
	if(obj==nullptr)
		return -1;
	else if(obj->isMoved())
		return obj->getNewAddr();

	int newAddr = 0;
	unsigned lastAccess = curTime - obj->timeStamp;

	if(obj->getAge()>2){
		if(childLeftIn(obj, EDEN) && rootSet.count(id)==0)
			remsetEden.insert(id);
		if(childLeftIn(obj, COLD) && rootSet.count(id)==0)
			remsetCold.insert(id);
		if(parent!=-1 && rootSet.count(parent)==0)
			remsetMature.insert(parent);
		newAddr = allocateMature(obj->getSize());
		if(-1==newAddr){
			//std::cout << "\nNot enough space in mature area, do GC.";
			return newAddr;
		}
		//obj->setMoved();
		edenSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		matureSpace->addObject(id, obj);
	}else{
		newAddr = allocateTo(obj->getSize());
		obj->setMoved();
		edenSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		toSpace->addObject(id, obj);
		worklist.push_back(id);
	}

	obj->incAge();
	return newAddr;
}

/*
 *	Moves object from Mature space to either edenSpace, toSpace or coldSpace
 *	Returns : address of object in moved location.
 */
int Space::moveMature(int id, int parent=-1){
	Object *obj = matureSpace->getObject(id);
	if(obj==nullptr)
		return -1;
	else if(obj->isMoved())
		return obj->getNewAddr();

	int newAddr = 0;
	unsigned lastAccess = curTime - obj->timeStamp;

	if(lastAccess > coldnessThreshold){
		if(childLeftIn(obj, EDEN) && rootSet.count(id)==0)
	 		remsetEden.insert(id);
		if(childLeftIn(obj, MATURE) && rootSet.count(id)==0)
	 		remsetMature.insert(id);
		if(parent!=-1 && rootSet.count(parent)==0)
			remsetCold.insert(parent);
		newAddr = allocateCold(obj->getSize());
		if(-1==newAddr){
			//std::cout << "\nNot enough space in cold area, do GC.";
			return newAddr;
		}
		//obj->setMoved();
		matureSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		coldSpace->addObject(id, obj);
	}else{
		newAddr = allocateTo(obj->getSize());
		obj->setMoved();
		matureSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		toSpace->addObject(id, obj);
		worklist.push_back(id);
	}

	obj->incAge();
	return newAddr;
}

/*
 *	Moves object from Cold space to either matureSpace, toSpace
 *	Returns : address of object in moved location.
 */
int Space::moveCold(int id, int parent=-1){
	Object *obj = coldSpace->getObject(id);
	if(obj==nullptr)
		return -1;
	else if(obj->isMoved())
		return obj->getNewAddr();

	int newAddr = 0;
	unsigned lastAccess = curTime - obj->timeStamp;

	if(lastAccess <= coldnessThreshold){ //revitalzation, moves to mature always
		if(childLeftIn(obj, COLD) && rootSet.count(id)==0)
	 		remsetCold.insert(id);
		if(childLeftIn(obj, EDEN) && rootSet.count(id)==0)
	 		remsetEden.insert(id);
		if(parent!=-1 && rootSet.count(parent)==0)
			remsetMature.insert(parent);
		newAddr = allocateMature(obj->getSize());
		if(-1==newAddr){
			//std::cout << "Not enough space in cold area, do GC.\n";
			return newAddr;
		}
		//obj->setMoved();
		coldSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		matureSpace->addObject(id, obj);
		revitalized++;
	}else{
		newAddr = allocateTo(obj->getSize());
		obj->setMoved();
		coldSpace->removeObject(id, false);		//don't remove object, just remove from map.
		obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
		toSpace->addObject(id, obj);
		worklist.push_back(id);
	}

	obj->incAge();
	return newAddr;
}

/*
 * Move children of object ID from Eden space.
 */
void Space::processMovedEden(int id){
	objectAllocationMap toMap = toSpace->getObjMap();
	Object *obj = toMap[id];

	int nrefs = obj->getNRefs();
	int *children = obj->getChildren();

	for(int i=0; i<nrefs; i++)
		if(exists(children[i], EDEN))
			moveEden(children[i], id);
}

/*
 * Move children of object ID from Mature space.
 */
void Space::processMovedMature(int id){
	objectAllocationMap toMap = toSpace->getObjMap();
	Object *obj = toMap[id];

	int nrefs = obj->getNRefs();
	int *children = obj->getChildren();

	for(int i=0; i<nrefs; i++)
		if(exists(children[i], MATURE))
			moveCold(children[i], id);
}

/*
 * Move children of object ID from Cold space.
 */
void Space::processMovedCold(int id){
	objectAllocationMap toMap = toSpace->getObjMap();
	Object *obj = toMap[id];

	int nrefs = obj->getNRefs();
	int *children = obj->getChildren();

	for(int i=0; i<nrefs; i++)
		if(exists(children[i], COLD))
			moveCold(children[i], id);
}

/*
 * Flip eden and to space.
 * Updates remsets, discards dead objects.
 */
void Space::flipEden(){
	objectAllocationMap toMap = toSpace->getObjMap();
	for(objectAllocationMap::iterator it=toMap.begin(); it!=toMap.end(); ++it){
		it->second->unsetMoved();
		it->second->setNewAddr(-1);
	}

	std::set<int>::iterator it;
	for (it=remsetCold.begin(); it!=remsetCold.end(); ++it){
		if(!exists(*it, TO))
			remsetCold.erase(it);
	}

	for (it=remsetMature.begin(); it!=remsetMature.end(); ++it){
		if(!exists(*it, TO))
			remsetMature.erase(it);
	}

	edenSpace->clear();

	Heap *tmp = edenSpace;
	edenSpace = toSpace;
	toSpace = tmp;
	delete toSpace;
	toSpace = nullptr;
}

/*
 * Flip mature and to space.
 * Updates remsets, discards dead objects.
 */
void Space::flipMature(){
	objectAllocationMap toMap = toSpace->getObjMap();
	for(objectAllocationMap::iterator it=toMap.begin(); it!=toMap.end(); ++it){
		it->second->unsetMoved();
		it->second->setNewAddr(-1);
	}

	std::set<int>::iterator it;
	for (it=remsetCold.begin(); it!=remsetCold.end(); ++it){
		if(!exists(*it, TO))
			remsetCold.erase(it);
	}

	for (it=remsetEden.begin(); it!=remsetEden.end(); ++it){
		if(!exists(*it, TO))
			remsetEden.erase(it);
	}

	matureSpace->clear();

	Heap *tmp = matureSpace;
	matureSpace = toSpace;
	toSpace = tmp;
	delete toSpace;
	toSpace = nullptr;
}

/*
 * Flip cold and to space.
 * Updates remsets, discards dead objects.
 */
void Space::flipCold(){
	objectAllocationMap toMap = toSpace->getObjMap();
	for(objectAllocationMap::iterator it=toMap.begin(); it!=toMap.end(); ++it){
		it->second->unsetMoved();
		it->second->setNewAddr(-1);
	}

	std::set<int>::iterator it;
	for (it=remsetEden.begin(); it!=remsetEden.end(); ++it){
		if(!exists(*it, TO))
			remsetEden.erase(it);
	}

	for (it=remsetMature.begin(); it!=remsetMature.end(); ++it){
		if(!exists(*it, TO))
			remsetMature.erase(it);
	}

	coldSpace->clear();

	Heap *tmp = coldSpace;
	coldSpace = toSpace;
	toSpace = tmp;
	delete toSpace;
	toSpace = nullptr;
}

int Space::gcCopyCollector(std::set<int> &rootSet){
	objectAllocationMap objMapFromSpace = fromSpace->getObjMap();

	//start with empty worklist.
	worklist.clear();

	for(int i:rootSet){
		if(exists(i, FROM))
			move(i);		//move will update worklist.
	}

	while(!worklist.empty()){
		int id = *worklist.begin();
		worklist.pop_front();
		if(exists(id, TO))
			processMovedObject(id);
	}

	flip();
	return 0;
}

int Space::move(int id){
	Object *obj = fromSpace->getObject(id);
	if(obj==nullptr)
		return -1;
	else if(obj->isMoved())
		return obj->getNewAddr();

	int newAddr = toSpace->allocate(obj->getSize());
	obj->setMoved();
	fromSpace->removeObject(id, false);		//don't remove object, just remove from map.
	obj->setAddr(newAddr);					//for simulation we don't actually use forwarding pointers.
	obj->incAge();							//update age to reflect number of GC survivals.
	toSpace->addObject(id, obj);
	worklist.push_back(id);
	return newAddr;
}

void Space::processMovedObject(int id){
	objectAllocationMap toMap = toSpace->getObjMap();
	Object *obj = toMap[id];

	int nrefs = obj->getNRefs();
	int *children = obj->getChildren();

	for(int i=0; i<nrefs; i++)
		if(exists(children[i], FROM))
			move(children[i]);
}

Space::Space(){
}

/*
 * Returns the name of output csv file.
 */
const char * Space::getOutputFile(){
	if(coldSpace)
		return "datatemp.csv";
	else
		return "copy.csv";
}

/*
 * Space constructor for Copy and ColdCopy collector.
 */
Space::Space(int sz, collector c){
	size = sz;
	switch (c)
    {
        case COPY:
        	fromSpace = new Heap(0, sz/2, COPY);
        	toSpace = new Heap(0, sz/2, COPY);
        	coldSpace = NULL;
        	break;
        case COLDCOPY:
        	fromSpace = nullptr;
        	edenSpace = new Heap(0, sz/4, COLDCOPY);
        	//toSpace = new Heap(0, sz/4, COLDCOPY);
        	matureSpace = new Heap(0, (1.5*sz/4), COLDCOPY);
        	coldSpace = new Heap(0, (0.3)*(sz/4), COLDCOPY);
        	break;
    }
}

/*
 * grows the size of each heap objects.
 */
void Space::grow(spaceType t){
	int increment=0;
	switch (t)
    {
        case FROM:
        	if(fromSpace){
        		increment = heapGrowth*fromSpace->getSize()/100;
        		fromSpace->grow(increment);
        	}
        	break;
        case TO:
        	if(toSpace){
        		increment = heapGrowth*toSpace->getSize()/100;
        		toSpace->grow(increment);
        	}
        	break;
        case COLD:
       		if(coldSpace){
        		increment = heapGrowth*coldSpace->getSize()/100;
       			coldSpace->grow(increment);
       		}
       		break;
        case EDEN:
        	if(edenSpace){
        		increment = heapGrowth*edenSpace->getSize()/100;
        		edenSpace->grow(increment);
        	}
        	break;
        case MATURE:
        	if(matureSpace){
        		increment = heapGrowth*matureSpace->getSize()/100;
        		matureSpace->grow(increment);
        	}
        	break;
    }
}

/*
 * grows the size of each heap objects.
 */
void Space::grow(){
	int increment = 0;
	if(coldSpace){
		increment = heapGrowth*edenSpace->getSize()/100;
		edenSpace->grow(increment);
		increment = heapGrowth*matureSpace->getSize()/100;
		matureSpace->grow(increment);
		increment = heapGrowth*coldSpace->getSize()/100;
		coldSpace->grow(increment);
	}else{
		increment = heapGrowth*fromSpace->getSize()/100;
		fromSpace->grow(increment);
		toSpace->grow(increment);
	}
}


/*
 * Getter functions for each heap type objects below.
 */
Heap* Space::getFromSpace(){
	return fromSpace;
}

Heap* Space::getEdenSpace(){
	return edenSpace;
}

Heap* Space::getMatureSpace(){
	return matureSpace;
}

Heap* Space::getToSpace(){
	return toSpace;
}

Heap* Space::getColdSpace(){
	return coldSpace;
}

/*
 * Allocation functions below specific to each heap type.
 */
int Space::allocateFrom(int size){
	return fromSpace->allocate(size);
}

int Space::allocateEden(int size){
	return edenSpace->allocate(size);
}

int Space::allocateMature(int size){
	return matureSpace->allocate(size);
}

int Space::allocateTo(int size){
	return toSpace->allocate(size);
}

int Space::allocateCold(int size){
	return coldSpace->allocate(size);
}

void Space::flip(){
	objectAllocationMap toMap = toSpace->getObjMap();
	for(objectAllocationMap::iterator it=toMap.begin(); it!=toMap.end(); ++it){
		it->second->unsetMoved();
		it->second->setNewAddr(-1);
	}

	fromSpace->clear();

	Heap *tmp = fromSpace;
	fromSpace = toSpace;
	toSpace = tmp;
}

/*
 * Locates and updates the time of object ID to currentTime.
 */
void Space::updateTime(int id){
	if(exists(id, EDEN))
		coldSpace->setTime(id);
	else if(exists(id, MATURE))
		edenSpace->setTime(id);
	else if(exists(id, COLD))
		matureSpace->setTime(id);
	else if(exists(id, TO))
		toSpace->setTime(id);
}

/*
 * Destructor for Space object.
 */
Space::~Space() {
	delete fromSpace;
	delete toSpace;
	delete matureSpace;
	delete coldSpace;
	fromSpace = toSpace = edenSpace  = matureSpace = coldSpace = NULL;
}

void Space::setColor(int id, Color c){
	Object *obj = fromSpace->getObject(id);
	obj->setColor(c);
}

Color Space::getColor(int id){
	Object *obj = fromSpace->getObject(id);
	return obj->getColor();
}

/*
 * Helper functions below.
 */
void Space::printObjMap(){
	objectAllocationMap from = fromSpace->getObjMap();
	objectAllocationMap to = toSpace->getObjMap();
	std::cout << "From : ";
	for(objectAllocationMap::iterator it=from.begin(); it!=from.end(); ++it){
		std::cout << it->first << " ";
	}
	std::cout << "\n";

	std::cout << "To : ";
	for(objectAllocationMap::iterator it=to.begin(); it!=to.end(); ++it){
		std::cout << it->first << " ";
	}
	std::cout << "\n";
}

void Space::printFreeList(){
	std::cout << "From : ";// << fromSpace->getStartAddr() << " <-> " << fromSpace->getSize() << "\n";
	fromSpace->showFreeList();
	std::cout << "To   : ";// << toSpace->getStartAddr() << " <-> " << toSpace->getSize() << "\n";
	toSpace->showFreeList();
}

void Space::printWorkList(){
	std::cout << "\nWork list : ";
	for(std::list<int>::iterator it=worklist.begin(); it!=worklist.end(); ++it)
		std::cout << *it << " ";
	std::cout << "\n\n";
}
