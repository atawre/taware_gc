/*
 * Space.h
 *
 *  Created on: 13-Feb-2018
 *      Author: ataware
 */

#ifndef SPACE_H_
#define SPACE_H_

#include "Heap.h"
#include "common.h"

enum spaceType{
	FROM,
	EDEN,
	MATURE,
	TO,
	COLD
};

class Space{
private:
	Heap *fromSpace, *toSpace, *edenSpace, *matureSpace, *coldSpace;
	int size;
	std::list<int> worklist;				 //for copy collector
	std::set<int> remsetFrom, remsetEden, remsetMature, remsetCold;

public:
	Space();
	Space(int sz, collector c);
	virtual ~Space();
	const char * getOutputFile();

	Heap *getFromSpace();
	Heap *getEdenSpace();
	Heap *getToSpace();
	Heap *getMatureSpace();
	Heap *getColdSpace();

	int allocateEden(int);
	int allocateTo(int);
	int allocateFrom(int);
	int allocateMature(int);
	int allocateCold(int);

	int gcCopyCollector(std::set<int> &rootSet);
	int move(int);
	void processMovedObject(int);
	void flip();

	int gcEdenSpace();
	int gcMatureSpace();
	int gcColdSpace();

	int moveEden(int, int);
	int moveMature(int, int);
	int moveCold(int, int);

	void processMovedEden(int);
	void processMovedMature(int);
	void processMovedCold(int);

	void flipEden();
	void flipMature();
	void flipCold();

	bool childLeftIn(Object *, spaceType);
	void addChild(int parentId, int childId, int slot);

	void grow();
	void grow(spaceType);

	void setTime(int);
	unsigned getTime(int);

	void printFreeList();
	void printObjMap();
	void printWorkList();

	bool exists(int, spaceType);

	Object * getObject(int id);

	void setColor(int , Color);
	Color getColor(int);

	void updateTime(int);
};

#endif /* SPACE_H_ */
