//
// Created by ataware on 13/4/18.
//

#ifndef ALLOCATOR_COMMON_H
#define ALLOCATOR_COMMON_H

extern std::set<int> rootSet;
extern unsigned curTime;
extern unsigned coldgcNum, edengcNum, maturegcNum, coldnessThreshold;
extern int revitalized;
extern int heapGrowth;

#endif //ALLOCATOR_COMMON_H
