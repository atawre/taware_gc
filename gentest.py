import sys
import os
import random

def genAlloc(count):
    bytes=0
    f=open("test", "w")
    for i in range(count):
        f.write("a T0 O"+str(i)+" S32 N3 C"+str(i)+"\n")
        f.write("+ T0 O"+str(i)+"\n")
        print "a T0 O"+str(i)+" S32 N3 C"+str(i)
        print "+ T0 O"+str(i)
        bytes += 32
    for i in range(count-1):
        print "w T0 P"+str(i)+" #0 O"+str(i+1)+" F10 S0 V0"
        f.write("w T0 P"+str(i)+" #0 O"+str(i+1)+" F10 S0 V0\n")
    for i in range(count):
        if(i%2):
            print "- T0 O"+str(i)
            f.write("- T0 O"+str(i)+"\n")

    #unlink element in the middle and point it to start.
    r = random.randint(1, count-1)
    f.write("w T0 P"+str(r)+" #0 O0 F10 S0 V0\n")
    f.write("a T0 O"+str(count)+" S32 N3 C"+str(count)+"\n") #excess object.
    print "w T0 P"+str(r)+" #0 O0 F10 S0 V0"
    f.close()
    os.rename("test", "test."+str(bytes))


def generate(count):
    genAlloc(count)

generate(int(sys.argv[1]))
